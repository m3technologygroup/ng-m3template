"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.injectEmulatorInitializer = void 0;
const ts = require("typescript");
const schematics_1 = require("@angular-devkit/schematics");
const change_1 = require("@schematics/angular/utility/change");
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
function injectEmulatorInitializer(options) {
    return (tree, _context) => {
        let changes = buildInjectionChanges(options, tree, _context);
        const declarationRecorder = tree.beginUpdate(`${options.path}/app.module.ts`);
        for (let change of changes) {
            if (change instanceof change_1.InsertChange) {
                declarationRecorder.insertLeft(change.pos, change.toAdd);
            }
        }
        tree.commitUpdate(declarationRecorder);
        return tree;
    };
}
exports.injectEmulatorInitializer = injectEmulatorInitializer;
;
function buildInjectionChanges(options, host, _context) {
    let text = host.read(`${options.path}/app.module.ts`);
    if (!text)
        throw new schematics_1.SchematicsException(`File ${options.path}/app.module.ts does not exist.`);
    let sourceText = text.toString('utf8');
    if (sourceText.indexOf('EmulatorLoader') >= 0) {
        _context.logger.warn("EmulatorLoader already part of namespace. Skipping....");
        return [new change_1.NoopChange()];
    }
    let sourceFile = ts.createSourceFile(`${options.path}/app.module.ts`, sourceText, ts.ScriptTarget.Latest, true);
    let nodes = (0, ast_utils_1.getSourceNodes)(sourceFile);
    let ctorNode = nodes.find(n => n.kind == ts.SyntaxKind.Constructor);
    let constructorChange = [];
    if (!ctorNode) {
        // No constructor found
        constructorChange.push(createConstructorForInjection(options, nodes));
    }
    else {
        constructorChange.push(...addConstructorArgument(options, ctorNode));
    }
    return [
        ...constructorChange,
        (0, ast_utils_1.insertImport)(sourceFile, `${options.path}/app.module.ts`, 'EmulatorLoader', 'cres-com-wrapper'),
        (0, ast_utils_1.insertImport)(sourceFile, `${options.path}/app.module.ts`, 'EmulatorData', 'src/assets/data/EmulatorData')
    ];
}
function createConstructorForInjection(options, nodes) {
    let classNode = nodes.find(n => n.kind === ts.SyntaxKind.ClassKeyword);
    if (!classNode) {
        throw new schematics_1.SchematicsException(`expected class in ${options.path}/app.module.ts`);
    }
    if (!classNode.parent) {
        throw new schematics_1.SchematicsException(`expected constructor in ${options.path}/app.module.ts</span> to have a parent node`);
    }
    let siblings = classNode.parent.getChildren();
    let classIndex = siblings.indexOf(classNode);
    siblings = siblings.slice(classIndex);
    let classIdentifierNode = siblings.find(n => n.kind === ts.SyntaxKind.Identifier);
    if (!classIdentifierNode) {
        throw new schematics_1.SchematicsException(`expected class in ${options.path}/app.module.ts to have an identifier`);
    }
    if (classIdentifierNode.getText() !== 'AppModule') {
        throw new schematics_1.SchematicsException(`expected first class in ${options.path}/app.module.ts to have the name AppModule`);
    }
    // Find opening cury braces (FirstPunctuation means '{' here).
    let curlyNodeIndex = siblings.findIndex(n => n.kind === ts.SyntaxKind.FirstPunctuation);
    siblings = siblings.slice(curlyNodeIndex);
    let listNode = siblings.find(n => n.kind === ts.SyntaxKind.SyntaxList);
    if (!listNode) {
        throw new schematics_1.SchematicsException(`expected first class in ${options.path}/app.module.ts to have a body`);
    }
    let toAdd = `
  constructor(private emulator: EmulatorLoader) {
    emulator.initEmulator(EmulatorData);
  }
`;
    return new change_1.InsertChange(`${options.path}/app.module.ts`, listNode.pos, toAdd);
}
function addConstructorArgument(options, ctorNode) {
    let siblings = ctorNode.getChildren();
    let parameterListNode = siblings.find(n => n.kind === ts.SyntaxKind.SyntaxList);
    if (!parameterListNode) {
        throw new schematics_1.SchematicsException(`expected constructor in ${options.path}/app.module.ts to have a parameter list`);
    }
    let parameterNodes = parameterListNode.getChildren();
    let paramNode = parameterNodes.find(p => {
        let typeNode = findSuccessor(p, [ts.SyntaxKind.TypeReference, ts.SyntaxKind.Identifier]);
        if (!typeNode)
            return false;
        return typeNode.getText() === `emulator`;
    });
    // There is already a respective constructor argument --> nothing to do for us here ...
    if (paramNode)
        return [new change_1.NoopChange()];
    //store changes so that we can add both injection and constructor body
    let changes = [];
    //first add the constructor body
    let blockListNode = siblings.find(n => n.kind === ts.SyntaxKind.Block);
    if (!blockListNode)
        throw new schematics_1.SchematicsException("expected constructor to have body");
    let blockOpenNode = blockListNode.getChildren().find(n => n.kind === ts.SyntaxKind.OpenBraceToken);
    let blockSyntaxList = blockListNode.getChildren().find(n => n.kind === ts.SyntaxKind.SyntaxList);
    let bodyAdd = "";
    if (blockSyntaxList.getText().length > 1) {
        bodyAdd = `\n    emulator.initEmulator(EmulatorData);`;
    }
    else {
        bodyAdd = `\n    emulator.initEmulator(EmulatorData);\n  `;
    }
    changes.push(new change_1.InsertChange(`${options.path}/app.module.ts`, blockOpenNode.pos + 2, bodyAdd));
    // Is the new argument the first one?
    if (!paramNode && parameterNodes.length == 0) {
        let toAdd = `private emulator: EmulatorLoader`;
        changes.push(new change_1.InsertChange(`${options.path}/app.module.ts`, parameterListNode.pos, toAdd));
    }
    else if (!paramNode && parameterNodes.length > 0) {
        let toAdd = `,
    private emulator: EmulatorLoader`;
        let lastParameter = parameterNodes[parameterNodes.length - 1];
        changes.push(new change_1.InsertChange(`${options.path}/app.module.ts`, lastParameter.end, toAdd));
    }
    return changes;
}
function findSuccessor(node, searchPath) {
    let children = node.getChildren();
    let next = undefined;
    for (let syntaxKind of searchPath) {
        next = children.find(n => n.kind == syntaxKind);
        if (!next)
            return null;
        children = next.getChildren();
    }
    return next;
}
//# sourceMappingURL=inject-emulator-initializer.js.map