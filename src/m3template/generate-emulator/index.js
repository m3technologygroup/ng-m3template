"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateEmulator = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const get_workspace_1 = require("../shared/get-workspace/get-workspace");
const create_emulator_template_1 = require("./create-emulator-template/create-emulator-template");
const inject_emulator_initializer_1 = require("./inject-emulator-initializer/inject-emulator-initializer");
//generates blank cues emulator cues and load the emulator on start
function generateEmulator(options) {
    return (0, schematics_1.chain)([
        (0, get_workspace_1.getWorkspace)(options),
        (0, inject_emulator_initializer_1.injectEmulatorInitializer)(options),
        (0, create_emulator_template_1.createEmulatorTempalte)(options)
    ]);
}
exports.generateEmulator = generateEmulator;
//# sourceMappingURL=index.js.map