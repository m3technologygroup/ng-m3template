"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.replaceStyles = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const path_1 = require("path");
function replaceStyles(options) {
    return (tree, _context) => {
        const tempalteSource = (0, schematics_1.apply)((0, schematics_1.url)('./files'), [
            (0, schematics_1.applyTemplates)({}),
            (0, schematics_1.move)((0, path_1.normalize)(`${options.rootPath}`))
        ]);
        return (0, schematics_1.branchAndMerge)((0, schematics_1.chain)([
            (0, schematics_1.mergeWith)(tempalteSource, schematics_1.MergeStrategy.Overwrite),
        ]))(tree, _context);
    };
}
exports.replaceStyles = replaceStyles;
//# sourceMappingURL=replace-styles.js.map