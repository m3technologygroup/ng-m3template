"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cleanHtmlCss = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const get_workspace_1 = require("../../shared/get-workspace/get-workspace");
const clean_html_1 = require("./clean-html");
const replace_styles_1 = require("./replace-styles");
//WHen we instal angular material, it adds webfont links (which don't work on a offline TP)
//as well as a bunch of CSS that we don't want. (we have our own custom material theme)
//this removes all of that, anf fixes it.
function cleanHtmlCss(options) {
    let Rules = [];
    //Get Workspace name and paths
    Rules.push((0, get_workspace_1.getWorkspace)(options));
    //Clean out the font refferences from index.html
    Rules.push((0, clean_html_1.CleanIndexHtml)(options));
    //Replace Styles.scss
    Rules.push((0, replace_styles_1.replaceStyles)(options));
    //chain the selected rules
    return (0, schematics_1.chain)([
        ...Rules
    ]);
}
exports.cleanHtmlCss = cleanHtmlCss;
//# sourceMappingURL=clean-html-css.js.map