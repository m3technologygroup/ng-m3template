"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CleanIndexHtml = void 0;
const schematics_1 = require("@angular-devkit/schematics");
//Rule to clean out the web font links that angular material adds to index.html
//Becase Crestron defices can be run offline, we can't use webfonts.
//we installed @fontsource/roboto and @fontsource/material-icons earlier
//so we can safeley remove any font links here.
function CleanIndexHtml(options) {
    return (tree, _context) => {
        var _a;
        //check for the file
        const filePath = `${options.rootPath}/index.html`;
        let tmp = (_a = tree.get(filePath)) === null || _a === void 0 ? void 0 : _a.content.toString();
        if (tmp == undefined || tmp.length == 0) {
            throw new schematics_1.SchematicsException("could not locate index.html");
        }
        let locatedLines = [];
        let searchIdx = 0;
        let runLoop = true;
        const searchStart = '<link';
        const searchEnd = '>';
        const linkMatch = 'https://fonts'; //if a link starts with this, we don't want it here
        while (runLoop) {
            //find the start
            let start = tmp.indexOf(searchStart, searchIdx);
            if (start >= 0) {
                //if start found find the end
                let end = tmp.indexOf(searchEnd, start);
                if (end < 0) //if we can't find the closing bracket, throw an exception
                    throw new schematics_1.SchematicsException(`HTML element invalid syntax in ${filePath}`);
                //place the end point as the start point for next loop itteration
                searchIdx = end;
                //get the link tag and check to make sire it is one we want to remove.
                const tag = tmp.substring(start, end);
                //check to see if hreff is a font
                if (tag.indexOf(linkMatch, 0) >= 0) {
                    locatedLines.push({ start: start, legnth: (1 + end - start) });
                }
            }
            else {
                //if no start, set runloop to false to exit
                runLoop = false;
            }
        }
        if (locatedLines.length <= 0) {
            _context.logger.warn(`Font Imports not found in ${filePath}. Skipping...`);
            return tree;
        }
        const exportRecorder = tree.beginUpdate(filePath);
        locatedLines.forEach(element => {
            exportRecorder.remove(element.start, element.legnth);
        });
        tree.commitUpdate(exportRecorder);
        return tree;
    };
}
exports.CleanIndexHtml = CleanIndexHtml;
//# sourceMappingURL=clean-html.js.map