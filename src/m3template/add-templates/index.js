"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addTempalteStyles = void 0;
const tasks_1 = require("@angular-devkit/schematics/tasks");
const dependencies_1 = require("@schematics/angular/utility/dependencies");
const NodeVersions_1 = require("../NodeVersions");
function addTempalteStyles(options) {
    return (host, context) => {
        //get version of angular material, should be the same version os @angular/core
        const ngCoreVersion = (0, dependencies_1.getPackageJsonDependency)(host, '@angular/core').version;
        //add packages
        (0, dependencies_1.addPackageJsonDependency)(host, {
            type: dependencies_1.NodeDependencyType.Default,
            name: "@fontsource/roboto",
            version: NodeVersions_1.FontsourceRobotoVersion,
            overwrite: false
        });
        (0, dependencies_1.addPackageJsonDependency)(host, {
            type: dependencies_1.NodeDependencyType.Default,
            name: "@fontsource/material-icons",
            version: NodeVersions_1.FontsourceMatIconVersion,
            overwrite: false
        });
        (0, dependencies_1.addPackageJsonDependency)(host, {
            type: dependencies_1.NodeDependencyType.Default,
            name: "m3-components",
            version: `bitbucket:m3technologygroup/m3-components#${NodeVersions_1.M3ComponentsVersion}`,
            overwrite: false
        });
        (0, dependencies_1.addPackageJsonDependency)(host, {
            type: dependencies_1.NodeDependencyType.Default,
            name: "@angular/material",
            version: ngCoreVersion,
            overwrite: false
        });
        const installTaskID = context.addTask(new tasks_1.NodePackageInstallTask());
        const setupTaskID = context.addTask(new tasks_1.RunSchematicTask("setup-template", options), [installTaskID]);
        context.addTask(new tasks_1.RunSchematicTask("clean-html-css", options), [setupTaskID]);
    };
}
exports.addTempalteStyles = addTempalteStyles;
/*
let Rules: Rule[] = [];

  Rules.push(getWorkspace(options));

  if(options['skip-install']) {
    throw new SchematicsException("addTemplateStyles does not support skip-install flag");
   }

   //install the necessary packages
   Rules.push(schematic("install-style-deps",{"project":options.project}));

  //Setup Angular Material
  Rules.push(externalSchematic('@angular/material','ng-add',{
    "project":options.project,
    "theme":"custom",
    "typography":false,
    "animations":true}));

  return chain([...Rules]);
*/ 
//# sourceMappingURL=index.js.map