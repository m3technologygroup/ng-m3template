"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setupTemplate = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const get_workspace_1 = require("../shared/get-workspace/get-workspace");
function setupTemplate(options) {
    let Rules = [];
    Rules.push((0, get_workspace_1.getWorkspace)(options));
    Rules.push((0, schematics_1.externalSchematic)('@angular/material', 'ng-add', {
        "project": options.project,
        "theme": "custom",
        "typography": false,
        "animations": true
    }));
    return (0, schematics_1.chain)([...Rules]);
}
exports.setupTemplate = setupTemplate;
//# sourceMappingURL=setup-template.js.map