"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPropertyNode = void 0;
const ts = require("typescript");
//looks for property nodes in a node arr, and checks to see if the property name matches the
//input 'property', if a match is found the node is returned, otherwise null is returned
function getPropertyNode(nodes, property) {
    for (let node of nodes) {
        if (node.kind == ts.SyntaxKind.PropertyAssignment) {
            let children = node.getChildren();
            let key = children.find(n => n.kind === ts.SyntaxKind.Identifier);
            if (key && key.getText() === property) {
                return node;
            }
        }
    }
    return null;
}
exports.getPropertyNode = getPropertyNode;
//# sourceMappingURL=get-property-node.js.map