import * as ts from 'typescript';
export declare function getPropertyNode(nodes: ts.Node[], property: string): ts.Node | null;
