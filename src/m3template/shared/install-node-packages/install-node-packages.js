"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.installNodePackages = void 0;
const tasks_1 = require("@angular-devkit/schematics/tasks");
//all it does is run an install task, that way it only happens once at the end.
function installNodePackages() {
    return (tree, _context) => {
        _context.addTask(new tasks_1.NodePackageInstallTask(), []);
        return tree;
    };
}
exports.installNodePackages = installNodePackages;
//# sourceMappingURL=install-node-packages.js.map