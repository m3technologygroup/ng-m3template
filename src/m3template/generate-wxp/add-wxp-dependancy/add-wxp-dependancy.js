"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addWxpDependancy = void 0;
const dependencies_1 = require("@schematics/angular/utility/dependencies");
const NodeVersions_1 = require("../../NodeVersions");
function addWxpDependancy() {
    return (tree, _context) => {
        const dep = {
            type: dependencies_1.NodeDependencyType.Default,
            name: '@crestron/ch5-webxpanel',
            version: NodeVersions_1.CrestronWebXpanelVersion,
            overwrite: true,
        };
        (0, dependencies_1.addPackageJsonDependency)(tree, dep);
        return tree;
    };
}
exports.addWxpDependancy = addWxpDependancy;
//# sourceMappingURL=add-wxp-dependancy.js.map