"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateWebXpanel = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const get_workspace_1 = require("../shared/get-workspace/get-workspace");
const install_node_packages_1 = require("../shared/install-node-packages/install-node-packages");
const add_wxp_dependancy_1 = require("./add-wxp-dependancy/add-wxp-dependancy");
const generate_wxp_provider_1 = require("./generate-wxp-provider/generate-wxp-provider");
const inject_wxp_provider_1 = require("./inject-wxp-provider/inject-wxp-provider");
//generates blank cues emulator cues and load the emulator on start
function generateWebXpanel(options) {
    let Rules = [];
    //Get workspace name and directories
    Rules.push((0, get_workspace_1.getWorkspace)(options));
    //Copy and tranform the provider tempalte into the project
    Rules.push((0, generate_wxp_provider_1.generareWxpProvider)(options));
    //INject the WXP provider into app.module.ts
    Rules.push((0, inject_wxp_provider_1.injectWxpProvider)(options));
    //Add the WXP NPM dependancy
    Rules.push((0, add_wxp_dependancy_1.addWxpDependancy)());
    //if skip install is set, skip the install process
    if (!options['skip-install']) {
        Rules.push((0, install_node_packages_1.installNodePackages)());
    }
    return (0, schematics_1.chain)([
        ...Rules
    ]);
}
exports.generateWebXpanel = generateWebXpanel;
//# sourceMappingURL=index.js.map