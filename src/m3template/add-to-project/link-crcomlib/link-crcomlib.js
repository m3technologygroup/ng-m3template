"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.linkCrComLib = void 0;
const schematics_1 = require("@angular-devkit/schematics");
function linkCrComLib(options) {
    return (tree, _context) => {
        const path = './angular.json';
        if (tree.exists(path)) {
            let currentAngularJson = tree.read(path).toString('utf8');
            let json = JSON.parse(currentAngularJson);
            let optionsJson = json['projects'][options.project]['architect']['build']['options'];
            //check to make sure the script isn't already there
            if (optionsJson['scripts'].includes("./node_modules/@crestron/ch5-crcomlib/build_bundles/umd/cr-com-lib.js")) {
                _context.logger.warn("CrComLib already linked to angular.json. Skipping...");
                return tree;
            }
            //add it
            optionsJson['scripts'].push("./node_modules/@crestron/ch5-crcomlib/build_bundles/umd/cr-com-lib.js");
            //write it
            json['projects'][options.project]['architect']['build']['options'] = optionsJson;
            //also fix the file size limits in angulat 12+ as the CrComLib is bloated and will trigger an error
            let budgetsJson = json['projects'][options.project]['architect']['build']['configurations']['production']['budgets'];
            let newBudgets = [];
            //find the initial budget and change it, that CresComLib is rather bloated
            for (let budget of budgetsJson) {
                if (budget['type'] === 'initial') {
                    budget['maximumWarning'] = '5mb';
                    budget['maximumError'] = '10mb';
                }
                if (budget['type'] === 'anyComponentStyle') {
                    budget['maximumWarning'] = '500kb';
                    budget['maximumError'] = '1mb';
                }
                newBudgets.push(budget);
            }
            //write it
            json['projects'][options.project]['architect']['build']['configurations']['production']['budgets'] = newBudgets;
            tree.overwrite(path, JSON.stringify(json, null, 2));
        }
        else {
            throw new schematics_1.SchematicsException("could not locate angular.json in root");
        }
        return tree;
    };
}
exports.linkCrComLib = linkCrComLib;
//# sourceMappingURL=link-crcomlib.js.map