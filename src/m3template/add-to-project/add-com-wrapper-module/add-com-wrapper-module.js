"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addComWrapperModule = void 0;
const ts = require("typescript");
const schematics_1 = require("@angular-devkit/schematics");
const change_1 = require("@schematics/angular/utility/change");
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
function addComWrapperModule(options) {
    return (tree, _context) => {
        let changes = buildInjectionChanges(options, tree, _context);
        const declarationRecorder = tree.beginUpdate(`${options.path}/app.module.ts`);
        for (let change of changes) {
            if (change instanceof change_1.InsertChange) {
                declarationRecorder.insertLeft(change.pos, change.toAdd);
            }
        }
        tree.commitUpdate(declarationRecorder);
        return tree;
    };
}
exports.addComWrapperModule = addComWrapperModule;
;
function buildInjectionChanges(options, host, _context) {
    let text = host.read(`${options.path}/app.module.ts`);
    if (!text)
        throw new schematics_1.SchematicsException(`File ${options.path}/app.module.ts does not exist.`);
    let sourceText = text.toString('utf8');
    if (sourceText.indexOf('cres-com-wrapper') >= 0) {
        _context.logger.warn("cres-com-wrapper already part of namespace. Skipping....");
        return [new change_1.NoopChange()];
    }
    let sourceFile = ts.createSourceFile(`${options.path}/app.module.ts`, sourceText, ts.ScriptTarget.Latest, true);
    return (0, ast_utils_1.addImportToModule)(sourceFile, 'CresComWrapperModule', 'CresComWrapperModule', 'cres-com-wrapper');
}
//# sourceMappingURL=add-com-wrapper-module.js.map