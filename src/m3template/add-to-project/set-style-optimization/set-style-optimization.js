"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setStyleOptimization = void 0;
const schematics_1 = require("@angular-devkit/schematics");
function setStyleOptimization(options) {
    return (tree, _context) => {
        const path = './angular.json';
        if (tree.exists(path)) {
            let currentAngularJson = tree.read(path).toString('utf8');
            let json = JSON.parse(currentAngularJson);
            let productionJson = json['projects'][options.project]['architect']['build']['configurations']['production'];
            //check to make sure the optimization flag isn't already there
            if (productionJson.hasOwnProperty("optimization")) {
                if (productionJson.hasOwnProperty("styles")) {
                    _context.logger.warn("Production Build Style Optimization already exists in angular.json. Skipping...");
                    return tree;
                }
                //just add the styles tag
                productionJson['optimization']['styles'] = false;
            }
            else {
                //add the while styles object
                productionJson['optimization'] = { styles: false };
            }
            //write it
            json['projects'][options.project]['architect']['build']['configurations']['production'] = productionJson;
            tree.overwrite(path, JSON.stringify(json, null, 2));
        }
        else {
            throw new schematics_1.SchematicsException("could not locate angular.json in root");
        }
        return tree;
    };
}
exports.setStyleOptimization = setStyleOptimization;
//# sourceMappingURL=set-style-optimization.js.map