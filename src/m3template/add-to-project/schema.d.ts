export interface Options {
    "skip-install": boolean;
    emulator: boolean;
    wxp: boolean;
    project: string;
    path: string;
    rootPath: string;
    themes: boolean;
}
