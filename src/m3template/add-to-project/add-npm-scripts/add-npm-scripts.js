"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addNpmScripts = void 0;
const schematics_1 = require("@angular-devkit/schematics");
function addNpmScripts(options) {
    return (tree, _context) => {
        if (options.project == null)
            throw new schematics_1.SchematicsException("Cannot add scripts to package.json for undefined project");
        //Get path to package.json
        const path = './package.json';
        const distFolder = `dist/`;
        const buildFolder = `dist/${options.project}/`;
        if (tree.exists(path)) {
            //scripts to add
            const ch5prep = `replace-in-files --string='type=\\\"module\\\"' --replacement='defer' ${buildFolder}index.html`;
            const ch5arc = `ch5-cli archive -p ${options.project} -d ${buildFolder} -o ${distFolder}`;
            const ch5build = `npm run build && npm run ch5prep && npm run ch5arc`;
            const ch5send = `ch5-cli deploy ${distFolder}${options.project}.ch5z -H 0.0.0.0 -t touchscreen -p`;
            let currentPackageJson = tree.read(path).toString('utf8');
            let json = JSON.parse(currentPackageJson);
            let scriptsJson = json['scripts'];
            if (scriptsJson['ch5prep'] == ch5prep ||
                scriptsJson['ch5arc'] == ch5arc ||
                scriptsJson['ch5build'] == ch5build ||
                scriptsJson['ch5send'] == ch5send) {
                _context.logger.warn("NPM Deployment scrips already exist in package.json. Skipping...");
                return tree;
            }
            //add the scripts
            scriptsJson['ch5prep'] = ch5prep;
            scriptsJson['ch5arc'] = ch5arc;
            scriptsJson['ch5build'] = ch5build;
            scriptsJson['ch5send'] = ch5send;
            //update the file
            json['scripts'] = scriptsJson;
            tree.overwrite(path, JSON.stringify(json, null, 2));
        }
        else {
            throw new schematics_1.SchematicsException("Could not locate package.json in project root");
        }
        return tree;
    };
}
exports.addNpmScripts = addNpmScripts;
//# sourceMappingURL=add-npm-scripts.js.map