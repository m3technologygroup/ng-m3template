"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addCrComLibDep = void 0;
const dependencies_1 = require("@schematics/angular/utility/dependencies");
const NodeVersions_1 = require("../../NodeVersions");
function addCrComLibDep() {
    return (tree, _context) => {
        //Add the CrComLib
        const crComLib = {
            type: dependencies_1.NodeDependencyType.Default,
            name: '@crestron/ch5-crcomlib',
            version: NodeVersions_1.CrestronCrComLibVersion,
            overwrite: true,
        };
        (0, dependencies_1.addPackageJsonDependency)(tree, crComLib);
        //Add the cres-com-wrapper
        const cresComWrapper = {
            type: dependencies_1.NodeDependencyType.Default,
            name: 'cres-com-wrapper',
            version: `bitbucket:m3technologygroup/m3crescomwrapper#${NodeVersions_1.CresComWrapperVersion}`,
            overwrite: true,
        };
        (0, dependencies_1.addPackageJsonDependency)(tree, cresComWrapper);
        return tree;
    };
}
exports.addCrComLibDep = addCrComLibDep;
//# sourceMappingURL=add-dependencies.js.map