"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemoveIndexBaseHreff = void 0;
const schematics_1 = require("@angular-devkit/schematics");
//import { RemoveChange } from '@schematics/angular/utility/change';
//Rule to remove the base hreff tag from index.html
//inorder to maintain compatability with Crestron touch panels.
//this need to be run in conjunction with AddHreffProvider
function RemoveIndexBaseHreff(options) {
    return (tree, _context) => {
        var _a;
        const searchString = `<base href="/">`;
        //check for the file
        const filePath = `${options.rootPath}/index.html`;
        let tmp = (_a = tree.get(filePath)) === null || _a === void 0 ? void 0 : _a.content.toString();
        if (tmp == undefined || tmp.length == 0) {
            throw new schematics_1.SchematicsException("could not locate index.html");
        }
        let start = tmp.indexOf(searchString);
        if (start == -1) {
            _context.logger.warn(`${searchString} not found in ${filePath}. Skipping...`);
            return tree;
        }
        const exportRecorder = tree.beginUpdate(filePath);
        //const removeChange = new RemoveChange(filePath,start,searchString);
        exportRecorder.remove(start, searchString.length);
        tree.commitUpdate(exportRecorder);
        return tree;
    };
}
exports.RemoveIndexBaseHreff = RemoveIndexBaseHreff;
//# sourceMappingURL=remove-index-base-hreff.js.map