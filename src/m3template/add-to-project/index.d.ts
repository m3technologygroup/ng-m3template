import { Rule } from '@angular-devkit/schematics';
import { Options } from "./schema";
export declare function addToProject(options: Options): Rule;
