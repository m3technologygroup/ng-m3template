"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.injectAppBaseHreffIntoAppComponent = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const change_1 = require("@schematics/angular/utility/change");
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
const get_property_node_1 = require("../../shared/get-property-node/get-property-node");
const ts = require("typescript");
//Rule to add a APP_BASE_HREFF provider to app.modile.ts
//this sets the base HREFF to './' inorder to maintain compatability with Crestron touch panels.
//this need to be run in conjunction with RemoveIndexBaseHreff
function injectAppBaseHreffIntoAppComponent(options) {
    return (tree, _context) => {
        let changes = buildInjectionChanges(options, tree, _context);
        const declarationRecorder = tree.beginUpdate(`${options.path}/app.module.ts`);
        for (let change of changes) {
            if (change instanceof change_1.InsertChange) {
                declarationRecorder.insertLeft(change.pos, change.toAdd);
            }
        }
        tree.commitUpdate(declarationRecorder);
        return tree;
    };
}
exports.injectAppBaseHreffIntoAppComponent = injectAppBaseHreffIntoAppComponent;
;
function buildInjectionChanges(options, host, _context) {
    let text = host.read(`${options.path}/app.module.ts`);
    if (!text)
        throw new schematics_1.SchematicsException(`File ${options.path}/app.module.ts does not exist.`);
    let sourceText = text.toString('utf8');
    if (sourceText.indexOf('APP_BASE_HREF') >= 0) {
        _context.logger.warn("APP_BASE_HREF already part of namespace. Skipping....");
        return [new change_1.NoopChange()];
    }
    let sourceFile = ts.createSourceFile(`${options.path}/app.module.ts`, sourceText, ts.ScriptTarget.Latest, true);
    //get the node for the providers property
    let providersNode = (0, get_property_node_1.getPropertyNode)((0, ast_utils_1.getSourceNodes)(sourceFile), 'providers');
    if (!providersNode)
        throw new schematics_1.SchematicsException(`Expected "providers" array to exist in ${options.path}/app.module.ts`);
    //get the node for the array of providers
    let providersArr = providersNode.getChildren().find(n => n.kind === ts.SyntaxKind.ArrayLiteralExpression);
    if (!providersArr)
        throw new schematics_1.SchematicsException(`Expected "providers" property to have array in ${options.path}/app.module.ts`);
    //get the body of the providers array
    let providersArrBody = providersArr.getChildren().find(n => n.kind === ts.SyntaxKind.SyntaxList);
    if (!providersArrBody)
        throw new schematics_1.SchematicsException(`Expected "providers" array to have a body in ${options.path}/app.module.ts`);
    //Add the provider
    const provider = "{provide: APP_BASE_HREF, useValue: \"./\"}";
    let build = "";
    //check to see if there are already other providers, if not, add a CR at the end
    if (providersArrBody.getText().length > 1) {
        build = provider;
        build = build.concat(",\n    ");
    }
    else {
        build = "\n    ".concat(provider);
        build = build.concat("\n");
    }
    const providerChange = new change_1.InsertChange(`${options.path}/app.module.ts`, providersArrBody.getStart(), build);
    return [
        providerChange,
        (0, ast_utils_1.insertImport)(sourceFile, `${options.path}/app.module.ts`, 'APP_BASE_HREF', '@angular/common'),
    ];
}
//# sourceMappingURL=add-hreff-provider.js.map