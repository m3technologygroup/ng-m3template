"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addToProject = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const get_workspace_1 = require("../shared/get-workspace/get-workspace");
const install_node_packages_1 = require("../shared/install-node-packages/install-node-packages");
const add_com_wrapper_module_1 = require("./add-com-wrapper-module/add-com-wrapper-module");
const add_dependencies_1 = require("./add-dependencies/add-dependencies");
const add_hreff_provider_1 = require("./add-hreff-provider/add-hreff-provider");
const link_crcomlib_1 = require("./link-crcomlib/link-crcomlib");
const remove_index_base_hreff_1 = require("./remove-index-base-hreff/remove-index-base-hreff");
const set_style_optimization_1 = require("./set-style-optimization/set-style-optimization");
const add_npm_scripts_1 = require("./add-npm-scripts/add-npm-scripts");
//Executed on ng add - Sets base HREFF (for TSW/TS compatability)
//Adds CrComLib From NPM, and adds reffernce to the CrComLib to angular.json
//Adds the M3 cres-com-wrapper module to app.module.ts
function addToProject(options) {
    let Rules = [];
    //Get Workspace name and paths
    Rules.push((0, get_workspace_1.getWorkspace)(options));
    //inject the provider
    Rules.push((0, add_hreff_provider_1.injectAppBaseHreffIntoAppComponent)(options));
    //remove the base hreff form index.html
    Rules.push((0, remove_index_base_hreff_1.RemoveIndexBaseHreff)(options));
    //add npm scripts to package.json
    Rules.push((0, add_npm_scripts_1.addNpmScripts)(options));
    //add the crcomlib script to angular.json
    Rules.push((0, link_crcomlib_1.linkCrComLib)(options));
    //Disable style optimization on production builds for TS/TSW panels
    Rules.push((0, set_style_optimization_1.setStyleOptimization)(options));
    //install CrComLib and CresComWrapper
    Rules.push((0, add_dependencies_1.addCrComLibDep)());
    //import the CresComWrapper module
    Rules.push((0, add_com_wrapper_module_1.addComWrapperModule)(options));
    //if the emulator flag is set, install the emulator
    if (options.emulator) {
        Rules.push((0, schematics_1.schematic)('cres-emulator', { 'skip-install': true, 'project': options.project }));
    }
    //if the wxp flag is set, install wxp
    if (options.wxp) {
        Rules.push((0, schematics_1.schematic)('web-xpanel', { 'skip-install': true, 'project': options.project }));
    }
    //if the themes flag is set, install @angular/material, m3-components and replace styles.scss
    if (options.themes) {
        Rules.push((0, schematics_1.schematic)('styles-tempalte', { 'project': options.project }));
    }
    //once everythgin is done, run npm install, 
    //but if we install themes, that has its own install task,
    //so we can skip
    if (!options['skip-install'] && !options.themes) {
        Rules.push((0, install_node_packages_1.installNodePackages)());
    }
    //chain the selected rules
    return (0, schematics_1.chain)([
        ...Rules
    ]);
}
exports.addToProject = addToProject;
//# sourceMappingURL=index.js.map