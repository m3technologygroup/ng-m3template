export declare const CresComWrapperVersion = "0.2.1";
export declare const M3ComponentsVersion = "0.2.1";
export declare const CrestronWebXpanelVersion = "^1.0.3";
export declare const CrestronCrComLibVersion = "^1.1.2";
export declare const FontsourceRobotoVersion = "^4.5.0";
export declare const FontsourceMatIconVersion = "^4.5.0";
