"use strict";
//This is a centeralized place to list versions of node modules installed by this schematic.
//this should be updated as new versions are released and tested for compatability
Object.defineProperty(exports, "__esModule", { value: true });
exports.FontsourceMatIconVersion = exports.FontsourceRobotoVersion = exports.CrestronCrComLibVersion = exports.CrestronWebXpanelVersion = exports.M3ComponentsVersion = exports.CresComWrapperVersion = void 0;
exports.CresComWrapperVersion = '0.2.1';
exports.M3ComponentsVersion = '0.2.1';
exports.CrestronWebXpanelVersion = '^1.0.3';
exports.CrestronCrComLibVersion = '^1.1.2';
exports.FontsourceRobotoVersion = '^4.5.0';
exports.FontsourceMatIconVersion = '^4.5.0';
//# sourceMappingURL=NodeVersions.js.map