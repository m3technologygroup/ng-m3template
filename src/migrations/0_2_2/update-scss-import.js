"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateScssImport = void 0;
const schematics_1 = require("@angular-devkit/schematics");
//import { RemoveChange } from '@schematics/angular/utility/change';
//Rule to update the scss import statement for prior versions of ng-m3tempalte
//as of angular 13 using '~' to refference node_modules is now deprecated.
function UpdateScssImport(options) {
    return (tree, _context) => {
        var _a;
        const searchString = `@use '~m3-components' as m3c;`;
        //check for the file
        const filePath = `${options.rootPath}/styles.scss`;
        let tmp = (_a = tree.get(filePath)) === null || _a === void 0 ? void 0 : _a.content.toString();
        if (tmp == undefined || tmp.length == 0) {
            throw new schematics_1.SchematicsException("could not locate styles.scss");
        }
        let start = tmp.indexOf(searchString);
        if (start == -1) {
            _context.logger.warn(`${searchString} not found in ${filePath}. Skipping...`);
            return tree;
        }
        const exportRecorder = tree.beginUpdate(filePath);
        exportRecorder.remove(start, searchString.length);
        exportRecorder.insertLeft(start, `@use '/node_modules/m3-components' as m3c;`);
        tree.commitUpdate(exportRecorder);
        return tree;
    };
}
exports.UpdateScssImport = UpdateScssImport;
//# sourceMappingURL=update-scss-import.js.map