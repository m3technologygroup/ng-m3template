"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrate0_2_2 = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const get_workspace_1 = require("../../m3template/shared/get-workspace/get-workspace");
const set_style_optimization_1 = require("../../m3template/add-to-project/set-style-optimization/set-style-optimization");
const add_npm_scripts_1 = require("../../m3template/add-to-project/add-npm-scripts/add-npm-scripts");
const update_scss_import_1 = require("./update-scss-import");
const add_dependencies_1 = require("../../m3template/add-to-project/add-dependencies/add-dependencies");
const tasks_1 = require("@angular-devkit/schematics/tasks");
//Migration from 0.1.0 to 0.2.2
//Adds angular 13 support, and updates dependancies
//adds deploy scripts, and fixes import in styles.scss
function migrate0_2_2(options) {
    let Rules = [];
    //Get Workspace name and paths
    Rules.push((0, get_workspace_1.getWorkspace)(options));
    //add npm scripts to package.json
    Rules.push((0, add_npm_scripts_1.addNpmScripts)(options));
    //Disable style optimization on production builds for TS/TSW panels
    Rules.push((0, set_style_optimization_1.setStyleOptimization)(options));
    //Update the legacy SCSS import from Angular v12 to v13
    Rules.push((0, update_scss_import_1.UpdateScssImport)(options));
    //Update CrComLib and CresComWrapper
    Rules.push((0, add_dependencies_1.addCrComLibDep)());
    //Install Node Packages
    Rules.push(() => {
        return (tree, _context) => {
            _context.addTask(new tasks_1.NodePackageInstallTask());
            return tree;
        };
    });
    //chain the selected rules
    return (0, schematics_1.chain)([
        ...Rules
    ]);
}
exports.migrate0_2_2 = migrate0_2_2;
//# sourceMappingURL=index.js.map