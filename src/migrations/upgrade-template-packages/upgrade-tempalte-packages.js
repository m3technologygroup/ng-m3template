"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addTempalteStyles = void 0;
const dependencies_1 = require("@schematics/angular/utility/dependencies");
const NodeVersions_1 = require("../../m3template/NodeVersions");
function addTempalteStyles(_options) {
    return (host, context) => {
        //Check for m3components, if its no there, skip all of this.
        //if it is there, upgrade all of the necessary packages
        if ((0, dependencies_1.getPackageJsonDependency)(host, 'm3-components') == null) {
            context.logger.info("It appears that m3components is not installed. Skipping m3components upgrade...");
            return host;
        }
        //add packages
        (0, dependencies_1.addPackageJsonDependency)(host, {
            type: dependencies_1.NodeDependencyType.Default,
            name: "@fontsource/roboto",
            version: NodeVersions_1.FontsourceRobotoVersion,
            overwrite: true
        });
        (0, dependencies_1.addPackageJsonDependency)(host, {
            type: dependencies_1.NodeDependencyType.Default,
            name: "@fontsource/material-icons",
            version: NodeVersions_1.FontsourceMatIconVersion,
            overwrite: true
        });
        (0, dependencies_1.addPackageJsonDependency)(host, {
            type: dependencies_1.NodeDependencyType.Default,
            name: "m3-components",
            version: `bitbucket:m3technologygroup/m3-components#${NodeVersions_1.M3ComponentsVersion}`,
            overwrite: true
        });
        return host;
    };
}
exports.addTempalteStyles = addTempalteStyles;
//# sourceMappingURL=upgrade-tempalte-packages.js.map