# Ng-m3template

M3tempalte is a library of schematics for angular that greatly simplifies the process of creating a new angular project to be used with a Crestron touch screen.

It is recommended that this is done directly after the angular project is created (ng new). m3template makes some large changes that could potentially break existing projects.

## Usage:
```ng add bitbucket:m3technologygroup/ng-m3template#0.2.1```

By default, m3template will prompt a user on how they want to setup the project.

# Version History
- 0.2.1 - updated versions of m3-components and cres-com-wrapper
- 0.2.0 - Upgrded for Angular 13.x support
- 0.1.0 - Initial Release, Angular 12.x support

## Add to Project
When the ng add command is used, m3template will do the following:

- Change the base HREFF of the application to ‘./’ for Crestron touch screen compatibility.
- Add the m3 cres-com-wrapper library to the project and declare the CresComWrapper module in app.module.ts
- Install the Crestron CH5-CrComLib from NPM and add the script to the scripts array in angular.json

### CH5 Emulator
M3tempalte can connect the Crestron CH5 Emulator automatically. This will also generate a blank EmulatorData.ts file with the correct schema specified to simplify the process of defining emulator cues.

To add the emulator to a project after it is created, issue:
`ng generate ng-m3template:cres-emulator`

### CH5 xPanel
M3temaplte can generate the necessary boilerplate code to use the Crestron Web xPanel library. M3template will generate a provider that contains the WebXpanel library and connection parameters. M3tempalte will also install the xPanel library from NPM.

To add a CH5 Xpanel to the project after it is created, issue:
`Ng generate ng-m3template:web-xpanel`


### M3 Components, Angular Material, and Styles
M3tempalte can simplify UI design by importing a library of prebuilt components and themes. M3tempalte add @angular/material to the project as well as the Roboto and material icon fonts as nom packages for offline use. The m3-components library will also be installed.
NOTE: in order to set up theming information, styles.scss will be overwritten. **If you have styles in styles.scss, make sure to make a git commit prior to installing ng-m3template.**

To add styles and components to the project after it has been created, issue:
`Ng generate ng-m3tempalte:styles-template`

