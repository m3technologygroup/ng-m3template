const path = require('path');
const nodeExternals = require('webpack-node-externals');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/m3template/index.js',
  output: {
    path: path.resolve(__dirname, 'dist/m3template'),
    filename: 'index.js',
    libraryTarget: 'commonjs2'
  },
  mode: 'production',
  target: 'node',
  externals: [
    nodeExternals({
      whitelist: ['schematics-utilities', 'npm-registry-client']
    })
  ],
  plugins: [
    new CopyWebpackPlugin(
      [
        {
          from: 'src/collection.json',
          to: '../collection.json',
          toType: 'file'
        }
      ],
      {}
    )
  ]
};